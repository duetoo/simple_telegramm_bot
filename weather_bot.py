from pyowm import OWM
from pyowm import exceptions
import telebot
import logging


TOKEN = "997159910:AAGSk1qvmhqsPR3DzIGyqwV8laobwFd4IzU"
bot = telebot.TeleBot(TOKEN)
logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)
owm = OWM(API_key="1408a993d8d8c822a1acb03c57a1bb93", language="ru")


def get_info_now(city: str) -> str:
    """

    :rtype: str
    """
    observer_weather = owm.weather_at_place(city)
    weather_now = observer_weather.get_weather()
    return weather_now


@bot.message_handler(commands=["start"])
def bot_commands(message):
    bot.send_message(message.chat.id, "Привет напиши название своего города по-русски и я поделюсь с тобой погодой")


@bot.message_handler(content_types=['text'])
def send_weather(message):
    try:
        weather_now = get_info_now(message.text.capitalize())
    except exceptions.api_response_error.NotFoundError:
        bot.send_message(message.chat.id, "Не могу найти, попробуйте другой населенный пункт")
    else:
        temperature_now = round(int(weather_now.get_temperature(unit="celsius")['temp']), 1)
        clouds = weather_now.get_detailed_status()
        humidity = weather_now.get_humidity()
        wind = round(int(weather_now.get_wind()['speed']), 1)
        bot.send_message(message.chat.id, parse_mode="HTML", text=f"<strong>{city}</strong>: \n"                                                              
                                                                  f"Температура "
                                                                  f"<strong>{temperature_now} \xb0C</strong> \n "
                                                                  f"{clouds}, \n"
                                                                  f"cкорость ветра <strong>{wind} м/с</strong>, \n"
                                                                  f"относительная влажность воздуха "
                                                                  f"<strong>{humidity}%</strong> ")


bot.polling()
